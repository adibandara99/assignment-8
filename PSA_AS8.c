#include <stdio.h>
struct Student
{
    char Name[50];
    char Subject[20];
    float Marks;
};
int main()
{
    int i;
    struct Student st[i];
    printf("Input Information of students\n");
    for(i=0;i<5;i++)
    {
        printf("Enter the name of the student: ");
        scanf("%s",st[i].Name);
        printf("Enter the subject: ");
        scanf("%s",st[i].Subject);
        printf("Enter the marks for the subject: ");
        scanf("%f",&st[i].Marks);
        printf("------------------------------------------\n");
    }
    printf("Display Information of Students\n");
    for(i=0;i<5;i++)
    {
        printf("Name = %s\nSubject = %s\nMarks = %f\n",st[i].Name,st[i].Subject,st[i].Marks);
        printf("------------------------------------------\n");
    }
    return 0;
}
